<?
require CLASSES . 'Situation.php';
require CLASSES . 'Objective.php';
$objective = Objective::get($id);
$items = Situation::get_all($id);
const GRID_ITEM = 'situation';
require TEMPLATE . 'situations.php';
