<?php
require_once CLASSES . 'Member.php';
require_once CLASSES . 'Objective.php';
$id = $_GET['id'];
$objective = Objective::get($id);
$items = Objective::get_messages($id);
const GRID_ITEM = 'message';
require_once TEMPLATE . 'chat.php';
