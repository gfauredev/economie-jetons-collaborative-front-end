<?php
require(__DIR__ . "/../commun/session.php");
require(__DIR__ . "/../commun/getter.php");
require(__DIR__ . "/fonction.php");


$e = getenfant($_GET['idenfant']);
$o = getobjectif($_GET['idobjectif']);
$nbrpointplacer = nbrpointplacer($_GET['idobjectif'], $_GET['idsituation']);

if (isNewSituation($_GET['idobjectif'], $_GET['idsituation'])) {
  startsituation($_SESSION['idmembre'], $_GET['idobjectif'], $_GET['idsituation']);
}

$id_situation = $_GET['idsituation'];
$id_objectif = $_GET['idobjectif'];
$id_membre = $_GET['idmembre'];
