<?php
require_once CLASSES . 'Child.php';
require_once CLASSES . 'Objective.php';
$child = Child::get($id);
$items = Child::get_objective_of_child($id);
const GRID_ITEM = 'objective';
require_once TEMPLATE . 'objectives.php';
