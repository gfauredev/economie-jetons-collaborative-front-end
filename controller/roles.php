<?php
if (!$_SESSION['admin']) alert();
require_once CLASSES . 'Role.php';
$items = Role::get_all();
const GRID_ITEM = 'role';
require_once TEMPLATE . 'roles.php';
