<?php
// Relative path to documentation
const DOCS = '../docs/';

if ($_SESSION['admin']) {
  require_once DOCS . 'admin.php';
} else {
  require_once DOCS . 'user.php';
}
