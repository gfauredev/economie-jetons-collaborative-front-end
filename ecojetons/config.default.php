<?php
// Define ressources relative location to index
const CLASSES = '../../api-ecojetons/class/';
const CONTROLLER = '../controller/';
const TEMPLATE = '../template/';

// Define existing (allowed) pages
const PAGES = [
  'default' => 'follow', // Global default page
  'coordinator' => 'coordinate', // Coordinators default page
  'admin' => 'children', // Administrators default page
  // Global pages, they dont need a parameter
  'members', 'profile', 'roles', 'docs',
  // Specific pages, they need ?id GET parameters
  'evaluation', 'objectives', 'team', 'chat',
  'situations', 'rewards', 'objectiverewards'
];
