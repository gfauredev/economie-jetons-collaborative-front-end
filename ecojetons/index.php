<?php
if (file_exists('config.php')) {
  require_once 'config.php';
} else {
  require_once 'config.default.php';
}
require_once 'lib.php';
session_start();
require CLASSES . 'Member.php';

// Parse the query string into $ACTION with 'param' => 'value'
$ARGS = explode('/', $_GET['action']);
$ACTION = [];
for ($i = 0; $i < count($ARGS) / 2; $i++) {
  $ACTION += [
    $ARGS[$i * 2] => $ARGS[$i * 2 + 1]
  ];
}
var_dump($ACTION);
die();

// If not logged, try to log with given username & password
if (
  !empty($_POST['username']) && !empty($_POST['password'])
) {
  // Remove html special chars from username input
  $username = trim(htmlspecialchars($_POST['username']));
  try {
    // Try to set session’s member id with given credentials
    Member::login($username, $_POST['password']);
  } catch (Exception $e) {
    // Warn if wrong credentials & redirect to login page
    alert('Identifiant ou mot de passe incorrect');
  }
}

if (isset($_SESSION['id'])) {
  if (isset($_GET['page']) && in_array($_GET['page'], PAGES)) {
    // Redirect to queried if logged
    route($_GET['page']);
  } else if (isset($_GET['page']) == 'logout') {
    // Logout if needed
    session_unset();
    session_destroy();
  } else {
    // Redirect to user’s default page if no or wrong page given
    if ($_SESSION['admin']) {
      route(PAGES['admin']);
    } else if ($_SESSION['coordo']) {
      route(PAGES['coordinator']);
    }
    route();
  }
}

if (isset($_GET['page'])) {
  if ($_GET['page'] == 'signin') {
    route('signin');
  } else if ($_GET['page'] != 'logout') {
    // Warns the user if he tried to access a page without being logged
    alert('Veuillez vous connecter avant d’accéder à l’application');
  }
}

route('login'); // Display login page else
