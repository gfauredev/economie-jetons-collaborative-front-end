<?php
// Can be called to display an alert message & return to default page
function alert(string $msg = 'Vous devez être administrateur pour accéder à cette page')
{
  header('Location: /?alert=' . urlencode($msg));
  die($msg);
}

// Minification function to remove garbage from HTML output
function minify(string $html)
{
  // Remove HTML comments
  $minified_html = preg_replace('/<!--(.|\s)*?-->/', '', $html);
  // Replace multiple whitespaces with a single space
  $minified_html = preg_replace('/\s+/', ' ', $minified_html);
  // Remove whitespaces around curly braces, semicolons, commas
  $minified_html = preg_replace('/\s?([{};,])\s?/', '$1', $minified_html);
  // Remove whitespaces around HTML tags
  $minified_html = preg_replace('/\s?(<[^>]+>)\s?/', '$1', $minified_html);

  return $minified_html;
}

// Display (require) the asked page’s modules
function route(string $page = PAGES['default'])
{
  if (isset($_GET['id'])) $id = $_GET['id'];

  // Start recording HTML output & execute proper controller
  ob_start();
  require_once CONTROLLER . $page . '.php';
  $main = ob_get_clean(); // Record HTML to pass it to master.php

  // Record HTML output again & execute master.php template
  ob_start();
  require_once TEMPLATE . 'master.php';

  // die(minify(ob_get_clean())); // Terminate, printing minified HTML
  die(ob_get_clean()); // Terminate, printing full HTML for test
}
