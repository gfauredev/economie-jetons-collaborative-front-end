<nav class="tabs">
  <a href="/docs">
    <i class="fa-solid fa-book"></i>
    <i class="text">Documentation</i>
  </a>
  <a href="/profile" <?= ($page == 'profile') ? 'class="current"' : '' ?>>
    <i class="fa-solid fa-user"></i>
    <i class="text">Profil</i>
  </a>
  <a href="/logout">
    <i class="fa-solid fa-right-from-bracket"></i>
    <i class="text">Deconnexion</i>
  </a>
</nav>
