<!DOCTYPE html>
<html lang="fr">

<head>
  <!-- Définit l’encodage des caractères en utf-8 (le meilleur) -->
  <meta charset="utf-8">
  <!-- Force le rendu du site sur toute la zone affichable, mieux -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Définit le titre de la page visionnée -->
  <title><?= ucfirst($page) ?> — Economie Jetons</title>

  <!-- Le CSS spécifique de l’application -->
  <link rel="stylesheet" href="/style.css">
  <!--  Police de caractères Fira -->
  <link rel="stylesheet" href="/fira.css">
  <!-- L’icone à afficher en petit dans l’onglet -->
  <link rel="icon" type="image/x-icon" href="/icon/32.webp">

  <!-- Fichier décrivant l’application et ses icones -->
  <link rel="manifest" type="text/json" href="/manifest.json">
  <!-- Le script permettant l’interactivité de l’application -->
  <!-- <script src="/script.js" async></script> -->

  <!-- Métadonnées additionnelles éventuelles -->
  <?= isset($META) ? '' : '' ?>

  <!-- Icones de font-awesome tels que ceux utilisés dans le header -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer">
</head>

<body>
  <?php if (isset($_GET['alert'])) # If an alert was throwed, alert then continue
    echo '<script>alert("' . $_GET['alert'] . '");</script>'; ?>
  <header>
    <a href="/">
      <img src="/icon/small-banner.avif" alt="Logo association Trisomie 21 Haute-Garonne">
    </a>
    <?php if (isset($_SESSION['id'])) require_once TEMPLATE . 'nav.php' ?>
  </header>
  <?php if (isset($_SESSION['id'])) require_once TEMPLATE . 'toolbar.php' ?>
  <main>
    <?= $main ?>
  </main>
</body>

</html>
