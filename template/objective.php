<h2><?= $i['entitled'] ?></h2>
<section>
  <h3>Informations</h3>
  <p><?= $i['nbr_token'] ?> jetons</p>
  <p><?= $i['duration'] . " " . $i['duration_type'] ?></p>
  <p>Priorité : <?= $i['priority'] ?></p>
  <p><img src="<?= $i['image'] ?>" alt="Image de l’objectif <?= $i['entitled'] ?>"></p>
  <p>Réussi à <?= Objective::get_success_rate($id) * 100 ?> %</p>
</section>

<footer>
  <?= $i['worked'] ? 'Travaillé' : 'Inactif' ?>
  <a href="/situations/<?= $i['id_objective'] . '-' . urlencode('entitled') ?>">Évaluation</a>
  <a href="/objectiverewards/<?= $i['id_objective'] . '-' . urlencode('entitled') ?>">Récompenses</a>
  <a href="/chat/<?= $i['id_objective'] . '-' . urlencode('entitled') ?>">Messagerie</a>
</footer>
