<?php if ($_GET['page'] != 'team') { ?>

  <h2 class="searchable"><?= $i['name'] . ' ' . $i['surname'] ?></h2>

  <section>
    <h3>Informations</h3>
    <p>Né(e) le :<span class="searchable"><?= $i['birth_date_format'] ?></span></p>
  </section>

  <section>
    <h3>Localisation</h3>
    <p>Adresse: <?= $i['address'] ?></p>
    <p>Ville: <?= $i['city'] ?></p>
    <p>Code postal: <?= $i['zip_code'] ?></p>
  </section>

  <section>
    <h3>Email</h3>
    <p><?= $i['email'] ?></p>
  </section>

<?php } else { ?>

  <h2 class="searchable"><?= $i['name'] . ' ' . $i['surname'] ?></h2>

  <section>
    <h3>Informations</h3>
    <?= $i['role'] ?>
  </section>

  <footer>
    Depuis le <?= $i['date_request_format'] ?>
  </footer>

<?php } ?>
