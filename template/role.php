<h3><?= $i['entitled'] ?></h3>

<?php if ($i['id_role'] != 1) { ?>
  <footer>
    <form action="." method="POST" id="form-<?= $i['id_role'] ?>">
      <button type="submit" name="DELETE" class="bad">
        <i class='fa-solid fa-trash'></i>
      </button>
    </form>
  </footer>
<?php } ?>
