<form action="." method="POST">
  <fieldset>
    <h1>Connexion</h1>
    <label>Nom d’utilisateur (courriel)
      <input type="text" name="username" placeholder="Nom d’utilisateur" autocomplete="username" required autofocus>
    </label>
    <label>Mot de passe
      <input type="password" name="password" placeholder="Mot de passe" autocomplete="current-password" required>
    </label>
    <button type='submit' class='good'>
      <i class='fa-solid fa-right-to-bracket'></i>
      Se connecter
    </button>
    <aside>Pas encore de compte ? <a href="/signin">en faire la demande</a></aside>
  </fieldset>
</form>
