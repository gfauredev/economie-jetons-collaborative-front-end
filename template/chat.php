<h1>Messages de l’objectif : <?= $objective['entitled'] ?></h1>
<?php require TEMPLATE . 'grid.php' ?>
<article>
  <h2>Envoyer un message</h2>
  <form method="POST" id="chat" action="/chat/<?= $objective['id_objective'] ?>">
    <input type="text" name="body" placeholder="Objet du message">
    <textarea name="body" placeholder="Corps du message"></textarea>
    <button type="submit" name="submitter" value="CREATE" class="good">
      <i class="fa-solid fa-paper-plane"></i>
    </button>
  </form>
</article>
