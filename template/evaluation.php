<!-- <body onload="ajoutPointBody(), decremantationTimerBody()"> -->
<article class="flex-center flex-column notunfolded">
  <h1>Objectif</h1>
  <h2><?= $o['intitule'] ?></h2>
  <h4>Progression: <span class="progress-number"><?= $nbrpointplacer ?></span> / <?= $o['nbjeton'] ?></h4>
  <progress class="progress-bar" value="<?= $nbrpointplacer ?>" max="<?= $o['nbjeton'] ?>"> </progress>
</article>

<article class="flex-center flex-column notunfolded">
  <form method="post" action="add.php" class="add" is="fonction-ajouter-un-point">
    <input type="hidden" name="idsituation" value="<?= $id_situation ?>">
    <input type="hidden" name="idobjectif" value="<?= $id_objectif ?>">
    <input type="hidden" name="idmembre" value="<?= $id_membre ?>">
    <button type="submit" name="add" class="anime good" id="ajouter-un-point">
      <i class="fa-solid fa-plus"></i>Ajouter 1 point
    </button>
  </form>
  <div id="timer"></div>
  <form method="post" action="remove.php" class="remove">
    <input type="hidden" name="idsituation" value="<?= $id_situation ?>">
    <input type="hidden" name="idobjectif" value="<?= $id_objectif ?>">
    <input type="hidden" name="idmembre" value="<?= $id_membre ?>">
    <button type="submit" name="remove" class="anime bad" id="retirer-un-point">
      <i class="fa-solid fa-minus"></i>Retirer 1 point
    </button>
  </form>
</article>
<article class="flex-center flex-column notunfolded">
  <a class="anime good" target="_blank" href=/ffichageenfant.php?idenfant=<?= $_GET['idenfant'] . "&idobjectif=" . $id_objectif . "&idsituation=" . $id_situation ?>">
    <i class="fa-solid fa-mobile-screen"></i>AFFICHAGE ENFANT
  </a>
  <h2>Temp restant : </h2>
  <div class="deplayliiiiine">
    <p id="timerJours"></p>
    <p id="timerHeures"></p>
    <p id="timerMinutes"></p>
    <p id="timerSecondes" data-secondetotale="<?= getTempsRestant($id_objectif, $id_situation); ?>"></p>
  </div>
</article>
