<div id="toolbar">
  <div id="search-bar">
    <i class="fa-solid fa-magnifying-glass"></i>
    <input type="search" placeholder="nom, prénom, date, courriel" autofocus>
  </div>
  <nav class="tabs">
    <a href="/follow" <?= ($page == 'follow') ? 'class="current"' : '' ?>>
      <i class="fa-solid fa-child"></i>
      <i class="text">Suivi</i>
    </a>
    <?php if ($_SESSION['coordo']) { ?>
      <a href="/coordinate" <?= ($page == 'coordination') ? 'class="current"' : '' ?>>
        <i class="fa-solid fa-hands-holding-child"></i>
        <i class="text">Coordination</i>
      </a>
    <?php } ?>
    <?php if ($_SESSION['admin']) { ?>
      <a href="/members" <?= ($page == 'members') ? 'class="current"' : '' ?>>
        <i class="fa-solid fa-users"></i>
        <i class="text">Membres</i>
      </a>
      <a href="/children" <?= ($page == 'children') ? 'class="current"' : '' ?>>
        <i class="fa-solid fa-children"></i>
        <i class="text">Enfants</i>
      </a>
      <a href="/roles" <?= ($page == 'roles') ? 'class="current"' : '' ?>>
        <i class="fa-solid fa-user-tag"></i>
        <i class="text">Rôles</i>
      </a>
    <?php } ?>
  </nav>
</div>
