<h2 class="searchable"><?= $i['name'] . ' ' . $i['surname'] ?></h2>

<section>
  <h3>Informations</h3>
  <p>Né(e) le :<span class="searchable"><?= $i['birth_date_format'] ?></span></p>
</section>

<section>
  <h3>Liens</h3>
  <a href="/objectives/<?= $i['id_child'] . '-' . $i['name'] . '-' . $i['surname'] ?>">Objectifs</a>
  <a href="/rewards/<?= $i['id_child'] . '-' . $i['name'] . '-' . $i['surname'] ?>">Récompenses</a>
  <a href="/team/<?= $i['id_child'] . '-' . $i['name'] . '-' . $i['surname'] ?>">Équipe</a>
</section>

<footer>
  <p>Code Invitation: <?= $i['invitation_code'] ?></p>
</footer>
