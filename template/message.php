<?php $send = Member::get($i['id_member']) ?>
<article class='message<?= ($i['id_member'] == $_SESSION['id']) ? ' our' : '' ?>'>
  <header><?= $i['subject'] ?></header>
  <p><?= $i['body'] ?></p>
  <footer>
    <span class="sender"><?= $send['surname'] . ' ' . $send['name'] ?></span>
    <span class="date-sent"><?= $i['timestamp'] ?></span>
  </footer>
</article>
