<h2 class="searchable"><?= $i['timestamp_format'] ?></h2>

<section>
  <h3>État</h3>
  <?php
  $s = Situation::get_state($id, $i['id_situation']);
  switch ($s) {
    case 1:
      echo 'Réussi !';
      break;
    case 2:
      echo 'Échoué';
      break;
    default:
      echo 'En cours';
      break;
  }
  ?>
  <?php
  $t = Situation::get_time_before_end($id, $i['id_situation']);
  if ($t > 0) { ?>
    <p>Temps restant : <?= $t ?></p>
  <?php } ?>
</section>

<footer>
  <h3>Points</h3>
  <?php $s = Situation::get_nbr_put($id, $i['id_situation']) > 1 ? 's' : '' ?>
  <p><?= Situation::get_nbr_put($id, $i['id_situation']) . " point$s placé$s sur " . Situation::get_nbr_max($id, $i['id_situation']) ?></p>
  <a href="/evaluation/<?= $id ?>/<?= $i['id_situation'] ?>">Évaluer</a>
</footer>
