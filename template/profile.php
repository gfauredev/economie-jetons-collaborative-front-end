<?php $display = $_GET['page'] == 'signin' ? 'placeholder="' : 'value="' ?>
<form action="." method="POST">
  <fieldset>
    <legend>Mes Informations</legend>

    <label>Nom
      <input type="text" name="name" required <?= $display . $u['name']; ?>">
    </label>

    <label>Prénom
      <input type="text" name="surname" <?= $display . $u['surname']; ?>">
    </label>

    <label>Date de naissance
      <input type="date" name="birth_date" required value="<?= $u['birth_date']; ?>">
    </label>

    <button type="submit" class="good">
      <i class='fa-solid fa-floppy-disk'></i>
      Enregistrer
    </button>
  </fieldset>
</form>

<form action="." method="POST">
  <fieldset>
    <legend>Adresse</legend>

    <label>Adresse
      <input type="text" name="address" <?= $display . $u['address']; ?>">
    </label>

    <label>Ville
      <input type="text" name="city" <?= $display . $u['city']; ?>">
    </label>

    <label>Code postal
      <input type="text" name="zip_code" pattern="[0-9][0-9][0-9][0-9][0-9]" <?= $display . $u['zip_code']; ?>">
    </label>

    <button type="submit" class="good">
      <i class='fa-solid fa-floppy-disk'></i>
      Enregistrer
    </button>
  </fieldset>
</form>

<form action="." method="POST">
  <fieldset>
    <legend>Mot de passe</legend>

    <label class="technical">Email actuel
      <input type="email" name="email" autocomplete="username" required>
    </label>

    <?php if ($_GET['page'] != 'signin') { ?>
      <label>Ancien mot de passe
        <input type="password" name="current_password" autocomplete="current-password" required>
      </label>
    <?php } ?>

    <label>Mot de passe
      <input type="password" name="new_password1" minlength="12" autocomplete="new-password" required>
    </label>

    <label>Vérifier mot de passe
      <input type="password" name="new_password2" minlength="12" autocomplete="new-password" required>
    </label>

    <button type="submit" class="good">
      <i class='fa-solid fa-floppy-disk'></i>
      Enregistrer
    </button>
  </fieldset>
</form>

<form action="." method="POST">
  <fieldset>
    <legend>Email</legend>

    <label>Email
      <input type="email" name="email" autocomplete="username" required>
    </label>

    <?php if ($_GET['page'] != 'signin') { ?>
      <label>Mot de passe
        <input type="password" name="current_password" autocomplete="current-password" required>
      </label>
    <?php } ?>

    <button type="submit" class="good">
      <i class='fa-solid fa-floppy-disk'></i>
      Enregistrer
    </button>
  </fieldset>
</form>
